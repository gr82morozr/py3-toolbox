# Python3 Toolbox 

A utility set for :
1 - Filesystem
2 - Time
3 - Print
4 - AWS
5 - Elasticsearch
6 - Image
7 - Network


For install :
> pip install py3toolbox 

For upgrade:
> pip install py3toolbox --upgrade -vvv  --no-cache-dir

For PyPi upload:
> python setup.py sdist upload -r pypi

