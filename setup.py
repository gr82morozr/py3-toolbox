from distutils.core import setup
setup(
  name          = 'py3toolbox',
  packages      = ['py3toolbox'],
  version       = '0.0.40',
  description   = 'A Python3 tools and utilities collection',
  author        = 'Fan Yang',
  author_email  = 'gr82morozr@gmail.com',
  url           = 'https://gr82morozr@bitbucket.org/gr82morozr/py3-toolbox.git',  
  download_url  = 'https://gr82morozr@bitbucket.org/gr82morozr/py3-toolbox.git', 
  keywords      = ['Utility', 'Tools' ], 
  classifiers   = [],
  install_requires=['maya']
)
